$(document).ready(function() {
    $("#keyword").val('bleach');
    $("#act").on("click", function(e) {
        var keyword_book = $("#keyword").val();
        console.log(keyword_book);
        var url_books = url_book_data + keyword_book;
        console.log(url_books);

        $.ajax({
            url: url_books,
            success: function(hasil) {
                console.log(hasil.items);
                var obj_hasil = $("#hasil");
                obj_hasil.empty();

                for(i = 0; i < hasil.items.length; i++) {
                    var tmp_title = hasil.items[i].volumeInfo.title;
                    if(hasil.items[i].volumeInfo.imageLinks){
                        var tmp_thumbnails = hasil.items[i].volumeInfo.imageLinks.thumbnail;
                    } else {
                        var tmp_thumbnails = "https://media2.giphy.com/media/3zhxq2ttgN6rEw8SDx/giphy.gif";
                    }
                    if(hasil.items[i].volumeInfo.authors) {
                        var tmp_author = hasil.items[i].volumeInfo.authors;
                    } else {
                        var tmp_author = 'The author is not available';
                    }
                    if(hasil.items[i].volumeInfo.description) {
                        var tmp_desc = hasil.items[i].volumeInfo.description;
                    } else {
                        var tmp_desc = 'The description is not available';
                    }
                    console.log(tmp_title);
                    obj_hasil.append(
                        '<ul>' +
                            '<div class="card mb-3 shadow rounded" style="max-width: 80%">' + 
                                '<div class="row no-gutters">' +
                                    '<div class="col-md-4">' +
                                        '<img src="' + tmp_thumbnails + '" class="card-img">' +
                                    '</div>' +
                                    '<div class="col-md-8">' +
                                        '<div class="card-body">' +
                                            '<h5 class="card-title">' + tmp_title + '</h5>' +
                                            '<p class="card-text">' + tmp_author + '</p>' +
                                            '<p class="card-text">' + tmp_desc + '</p>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</ul>'
                    )
                }
            }
        });
    });
    $("#act").click();
});
from django.db import models

# Create your models here.

class Book (models.Model):
    id = models.CharField(max_length = 200, primary_key = True)
    title = models.CharField(max_length = 200)
    thumbnail = models.CharField(max_length = 100)
    author = models.CharField(max_length = 100, blank = True)
    description = models.CharField(max_length = 200)


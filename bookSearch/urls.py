from django.urls import path
from . import views

app_name = "bookSearch"

urlpatterns = [
	path('', views.index, name = 'index'),
    path('book-data/', views.bookData, name = 'book_data')
]

from django.shortcuts import render
from django.http import JsonResponse
import json, requests
from .models import Book
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

@csrf_exempt

def index(request):
    return render(request, 'index.html')

def bookData(request):
    try:
        q = request.GET['q']
    except:
        q = 'Not Found'

    response = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + q).json()

    return JsonResponse(response)
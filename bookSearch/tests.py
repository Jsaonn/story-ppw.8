from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.

class Test(TestCase):
    def test_app_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_search_url_ada(self):
        response = Client().get('/book-data/')
        self.assertEqual(response.status_code, 200)

    def test_app_ada(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page(self):
        self.assertIsNotNone(index)